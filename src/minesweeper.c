﻿#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>

#define Width_Max (10)
#define Height_Max (5)

#define Game_Start (1)
#define Game_Over (2)
#define Game_Clear (3)

typedef struct {
	// 付近の地雷数
	// 毎回計算しなくて良いように、解放時に地雷数を保持しておく
	int near;
	bool mine;
	bool opened;
	bool flagged;
} Math;

typedef struct {
	int x;
	int y;
} Player;

int state;
Player player;
Math math[Height_Max][Width_Max];

// TODO 関数定義がゴチャゴチャしているので、プロトタイプ定義か外部ファイルへ移動させる

// 範囲外アクセスチェックのため関数定義
Math * getMath(int x, int y)
{
	assert(x >= 0 && x < Width_Max);
	assert(y >= 0 && y < Height_Max);

	return &math[y][x];
}

char * getStateText(int state)
{
	switch (state) {
	case Game_Start:
		return "Game Start";
	case Game_Over:
		return "Game Over";
	case Game_Clear:
		return "Game Clear";
	default:
		return "";
	}
	assert(true);
}

// 確実に指定した地雷数を設定するために
// マス目の初期化コードと別に地雷設定を行なっている
void initializeMine(int mineCount)
{
	while(mineCount >= 0) {
		int x = rand() % Width_Max;
		int y = rand() % Height_Max;

		if (!getMath(x, y)->mine) {
			--mineCount;
			getMath(x, y)->mine = true;
		}
	}
}

void initialize(int mineCount)
{
	player.x = 0;
	player.y = 0;

	state = Game_Start;

	srand((unsigned)time(NULL));

	// TODO 構造体配列の初期化をもっと手軽に行なえないのか
	for (int i = 0; i < Height_Max; ++i) {
		for (int j = 0; j < Width_Max; ++j) {
			getMath(j, i)->mine = false;
			getMath(j, i)->opened = false;
			getMath(j, i)->flagged = false;
		}
	}
	initializeMine(mineCount);
}

void draw()
{
	// 画面クリアと座標固定のエスケープシーケンス
	printf("\033[2J");

	// ヘッダー情報の描画
	printf("\033[0;0H");
	printf("State : %s", getStateText(state));

	printf("\033[10;0H");
	for (int i = 0; i < Height_Max; ++i) {
		for (int j = 0; j < Width_Max; ++j) {
			if (j == player.x && i == player.y) {
				// プレイヤーの座標
				// TODO 自身座標の状態が見えないので対策が必要
				printf("P");
			} else if (!!getMath(j, i)->opened) {
				if (!!getMath(j, i)->mine) {
					// 地雷
					printf("x");
				} else {
					// 開封済みなため、付近の地雷数表示
					printf("%d", getMath(j, i)->near);
				}
			} else if (!!getMath(j, i)->flagged) {
				// プレイヤーが地雷予想で立てるフラグ
				printf("F");
			} else {
				// 未開封のマス目
				printf("-");
			}
		}
		printf("\n");
	}
}

int getNearMine(int x, int y)
{
	int count = 0;
	int top = y - 1;
	int left = x - 1;
	int bottom = y + 1;
	int right = x + 1;

	// TODO コメントで説明は補って、top, left, bottom, rightはわざわざ定義する必要はなさそう
	for (int i = top; i <= bottom; ++i) {
		for (int j = left; j <= right; ++j) {
			// TODO 不要な場合でも演算させているので、コストかかるようであればifにする
			count += (i >= 0 && j >= 0 && j < Width_Max && i < Height_Max && !!getMath(j, i)->mine) ? 1 : 0;
		}
	}
	return count;
}

// TODO ゲームオーバー時に呼び出させる
void openAll()
{
	for (int i = 0; i < Height_Max; ++i) {
		for (int j = 0; j < Width_Max; ++j) {
			getMath(j, i)->opened = true;
			getMath(j, i)->near = getNearMine(j, i);
		}
	}
}

bool isAutoOpened(int x, int y)
{
	if (x >= 0 && x < Width_Max && y >= 0 && y < Height_Max) {
		Math * math = getMath(x, y);
		int count = getNearMine(x, y);

		return !math->opened && count == 0;
	}
	return false;
}

bool isClear()
{
	for (int i = 0; i < Height_Max; ++i) {
		for (int j = 0; j < Width_Max; ++j) {
			Math * math = getMath(j, i);
			if (!math->opened && !math->mine) {
				return false;
			}
		}
	}
	return true;
}

void open(int x, int y)
{
	Math * math = getMath(x, y);

	// フラグを立てている場合は間違って開けないようにする
	if (!math->flagged) {
		math->opened = true;
		math->near = getNearMine(x, y);

		if (!!math->mine) {
			state = Game_Over;
		} else if (!!isClear()) {
			state = Game_Clear;
		}

		if (!!isAutoOpened(x - 1, y)) {
			open(x - 1, y);
		}
		if (!!isAutoOpened(x + 1, y)) {
			open(x + 1, y);
		}
		if (!!isAutoOpened(x, y - 1)) {
			open(x, y - 1);
		}
		if (!!isAutoOpened(x, y + 1)) {
			open(x, y + 1);
		}
	}
}

void input()
{
	// TODO 入力値の初期化はこれで大丈夫？
	char input[1] = "";
	scanf("%s", input);

	if (state == Game_Start) {
		if (*input == 'w') {
			// 上に1マス移動
			player.y -= (player.y > 0) ? 1 : 0;
		} else if (*input == 'a') {
			// 左に1マス移動
			player.x -= (player.x > 0) ? 1 : 0;
		} else if (*input == 's') {
			// 下に1マス移動
			player.y += (player.y < Height_Max - 1) ? 1 : 0;
		} else if (*input == 'd') {
			// 右に1マス移動
			player.x += (player.x < Width_Max - 1) ? 1 : 0;
		} else if (*input == 'f') {
			// 地雷予想のフラグを立てる
			getMath(player.x, player.y)->flagged = !getMath(player.x, player.y)->flagged;
		} else if (*input == 'q') {
			// デバッグ機能として一時的に定義
			openAll();
		} else if (*input == 'o' || *input == 'e') {
			open(player.x, player.y);
		}
	} else if (state == Game_Over) {
		if (*input == 'r') {
			initialize(10);
		}
	}
}

void update()
{
	input();
	draw();
}

int main()
{
	// TODO モードによって地雷数は変動させる
	// そのために定数定義なく 10 の固定値を渡している
	initialize(10);

	// updateで先に入力待ちが入るので描画を行なっておく
	// ただ、何か浮いてみえるので良い方法を探す
	draw();

	while (1) {
		update();
	}
	return 0;
}
